#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdio.h>

using namespace std;

double epsilon = 0.0000000001;

class Dim3Vector {
public:
	Dim3Vector(double x, double y, double z) : _x(x), _y(y), _z(z) {}
	Dim3Vector() : _x(0), _y(0), _z(0) {}

	friend Dim3Vector operator-(const Dim3Vector& vector_1, const Dim3Vector& vector_2) {
		return Dim3Vector(
			vector_1._x - vector_2._x, 
			vector_1._y - vector_2._y, 
			vector_1._z - vector_2._z
		);
	}

	friend Dim3Vector operator+(const Dim3Vector& vector_1, const Dim3Vector& vector_2) {
		return Dim3Vector(
			vector_1._x + vector_2._x,
			vector_1._y + vector_2._y,
			vector_1._z + vector_2._z
		);
	}

	friend double operator*(const Dim3Vector& vector_1, const Dim3Vector& vector_2) {
		return vector_1._x * vector_2._x + vector_1._y * vector_2._y + vector_1._z * vector_2._z;
	}

	friend Dim3Vector operator*(const Dim3Vector& vector_1, double scalar) {
		return Dim3Vector(
			vector_1._x * scalar,
			vector_1._y * scalar,
			vector_1._z * scalar
		);
	}

	double norm() const {
		return sqrt(_x * _x + _y * _y + _z * _z);
	}

	friend istream& operator>>(istream& input, Dim3Vector& dim3Vector) {
		double x, y, z;
		input >> x >> y >> z;
		dim3Vector = Dim3Vector(x, y, z);
		return input;
	}

private:
	double _x;
	double _y;
	double _z;
};



double distanceFromPoint(const Dim3Vector& point, const Dim3Vector& beginLine, const Dim3Vector& vectorLine) {
	Dim3Vector firstShiftDifference = point - beginLine;
	Dim3Vector secondShiftDifference = point - beginLine - vectorLine;
	if (vectorLine.norm() < epsilon) {
		return firstShiftDifference.norm();
	}
	double t = (firstShiftDifference * vectorLine) / (vectorLine * vectorLine);
	double pointStraightDistance = (point - beginLine - (vectorLine * t)).norm();
	if (0 < t && t < 1 && pointStraightDistance < min(firstShiftDifference.norm(), secondShiftDifference.norm()))
	{
		return pointStraightDistance;
	}
	return min(firstShiftDifference.norm(), secondShiftDifference.norm());
}



double ternarySearch(const Dim3Vector& beginFirstLine, const Dim3Vector& vector_1,
	const Dim3Vector& beginSecondLine, const Dim3Vector& vector_2)
{
	double leftBound = 0, rightBound = 1;
	while ((rightBound - leftBound) >= epsilon) {
		double firstMedian = leftBound + (rightBound - leftBound) / 3;
		double secondMedian = rightBound - (rightBound - leftBound) / 3;
		Dim3Vector firstShift = beginFirstLine + vector_1 * firstMedian;
		Dim3Vector secondShift = beginFirstLine + vector_1 * secondMedian;
		if (distanceFromPoint(firstShift, beginSecondLine, vector_2) < 
		    distanceFromPoint(secondShift, beginSecondLine, vector_2)) {
			rightBound = secondMedian;
		}
		else {
			leftBound = firstMedian;
		}
	}
	return distanceFromPoint(beginFirstLine + vector_1 * leftBound, beginSecondLine, vector_2);
}




int main() {
	Dim3Vector beginFirstLine, endFirstLine, beginSecondLine, endSecondLine;
	cin >> beginFirstLine >> endFirstLine;
	cin >> beginSecondLine >> endSecondLine;
	Dim3Vector vector_1 = endFirstLine - beginFirstLine;
	Dim3Vector vector_2 = endSecondLine - beginSecondLine;
	double distance = ternarySearch(beginFirstLine, vector_1, beginSecondLine, vector_2);
	printf("%0.11f", distance);
	system("pause");
}