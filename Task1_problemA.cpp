/*
Задача A. Найдите все вхождения шаблона в строку.
Найдите все вхождения шаблона в строку. Длина шаблона – p, длина строки – n.
Время O(n + p), доп. память – O(p).
p <= 30000, n <= 300000.

Решение. Используем алгоритм Кнута-Морриса-Прата, причем значение префикс-функции
будем хранить только для символов шаблона плюс один дополнительный символ
разделителя. Это имеет смысл, т.к. префикс-функция не может превысить длину шаблона.

Время - O(n + p)
Доп. память - O(p)
*/

#include<iostream>
#include<string>
#include<vector>
using namespace std;


vector<size_t> calculatePrefixFunction(const string& processingString) {
	vector<size_t> prefixFunction(processingString.size());
	prefixFunction[0] = 0;
	for (int i = 1; i < processingString.size(); i++) {
		int j = prefixFunction[i - 1];
		while (j > 0 && processingString[i] != processingString[j]) 
		{
			j = prefixFunction[j - 1];
		}
		if (processingString[i] == processingString[j])
		{
			j++;
		}
		prefixFunction[i] = j;
	}
	return prefixFunction;
}


void findAndPrintInsertions(const string& pattern, const string& processingString) {
	//добавляем разделитель к шаблону
	string updatedPattern = pattern + '#';
	
	//считаем префикс-функцию шаблона с разделителем
	vector<size_t> prefixFunction = calculatePrefixFunction(updatedPattern);
	int currentPrefixFunctionValue = 0;   //значение префикс-функции для обрабатываемого символа
	for (int i = 0; i < processingString.size(); i++) {
		int j = currentPrefixFunctionValue;
		while (j > 0 && processingString[i] != updatedPattern[j])
		{
			j = prefixFunction[j - 1];
		}
		if (processingString[i] == updatedPattern[j]) 
		{
			j++;
		}
		currentPrefixFunctionValue = j;
		if (currentPrefixFunctionValue == pattern.size()) 
		{
			cout << (i - j + 1) << ' ';
		}
	}
}

int main() {
	string theTemplate, mainString;
	cin >> theTemplate >> mainString;
	findAndPrintInsertions(theTemplate, mainString);
	return 0;
}