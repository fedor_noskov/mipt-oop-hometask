#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class BigInteger {
public:
	BigInteger();
	~BigInteger();
	BigInteger(BigInteger&& value) = default;
	BigInteger(const BigInteger& value) = default;

	explicit BigInteger(const string& value);
	BigInteger(const int value);

	BigInteger& operator=(const BigInteger& value) = default;
	BigInteger& operator=(BigInteger&& value) = default;

	string toString() const;

	// Логика и порядок.
	bool operator==(const BigInteger& other) const;
	bool operator!=(const BigInteger& other) const;
	bool operator<(const BigInteger& other) const;
	bool operator<=(const BigInteger& other) const;
	bool operator>(const BigInteger& other) const;
	bool operator>=(const BigInteger& other) const;

	//	унарный оператор
	BigInteger operator-() const;

	//	pre/post increment/decrement
	BigInteger& operator++();
	BigInteger& operator--();

	BigInteger operator++(int);
	BigInteger operator--(int);

	//	бинарные операторы
	friend BigInteger operator+(const BigInteger& left, const BigInteger& right);
	friend BigInteger operator*(const BigInteger& left, const BigInteger& right);
	friend BigInteger operator-(const BigInteger& left, const BigInteger& right);
	friend BigInteger operator/(const BigInteger& left, const BigInteger& right);
	friend BigInteger operator%(const BigInteger& left, const BigInteger& right);

	//	составные операторы
	BigInteger& operator+=(const BigInteger& other);
	BigInteger& operator-=(const BigInteger& other);
	BigInteger& operator*=(const BigInteger& other);
	BigInteger& operator/=(const BigInteger& other);
	BigInteger& operator%=(const BigInteger& other);

	//	преобразование типов
	explicit operator bool() const;

	//	ввод/вывод
	friend ostream& operator<<(ostream &stream, const BigInteger& value);
	friend istream& operator>>(istream &stream, BigInteger& value);

private:

	int _sign = 0;
	vector<int> _vectorRepresentation = { 0 };

	friend long long int _getFirstPositionForDivision(const BigInteger& term_1, const BigInteger& term_2) {
		if (term_1._vectorRepresentation.size() < term_2._vectorRepresentation.size()) {
			return -1;
		}
		bool more = false;
		bool equal = true;
		long long int sizeDifference = term_1._vectorRepresentation.size() -
			term_2._vectorRepresentation.size();
		for (long long int index_1 = term_1._vectorRepresentation.size() - 1;
			index_1 >= sizeDifference;
			index_1--) {
			long long int index_2 = index_1 - sizeDifference;
			if (term_1._vectorRepresentation[index_1] > term_2._vectorRepresentation[index_2]) {
				more = true;
				equal = false;
				break;
			}
			if (term_1._vectorRepresentation[index_1] < term_2._vectorRepresentation[index_2]) {
				more = false;
				equal = false;
				break;
			}
		}
		if (more || equal) {
			return sizeDifference;
		}
		return sizeDifference - 1;
	}


	friend pair<BigInteger, BigInteger> _division(const BigInteger& term_1, const BigInteger& term_2) {
		long long int index = _getFirstPositionForDivision(term_1, term_2);
		BigInteger result;
		BigInteger divinder = term_2;
		divinder._sign = 0;
		if (index == -1) {
			BigInteger surplus = term_1;
			surplus._sign = term_1._sign != term_2._sign;
			return make_pair<BigInteger, BigInteger>(move(result), move(surplus));
		}
		result._vectorRepresentation = {};
		BigInteger currentDividend;
		currentDividend._vectorRepresentation = {};
		currentDividend._vectorRepresentation.insert(
			currentDividend._vectorRepresentation.begin(),
			term_1._vectorRepresentation.begin() + index,
			term_1._vectorRepresentation.end()
		);


		while (index >= 0) {
			int digit = 0;
			while (!currentDividend._absLessThan(term_2)) {
				digit++;
				currentDividend = currentDividend - divinder;
			}
			result._vectorRepresentation.insert(
				result._vectorRepresentation.begin(),
				digit
			);
			index--;
			if (index >= 0) {
				if (currentDividend._isZero()) {
					currentDividend._vectorRepresentation = {};
				}
				currentDividend._vectorRepresentation.insert(
					currentDividend._vectorRepresentation.begin(),
					term_1._vectorRepresentation[index]
				);
			}
		}
		result._sign = term_1._sign != term_2._sign;
		currentDividend._sign = result._sign;
		return make_pair<BigInteger, BigInteger>(move(result), move(currentDividend));
	}


	bool _absLessThan(const BigInteger& term_2) const {
		if (_vectorRepresentation.size() > term_2._vectorRepresentation.size()) {
			return false;
		}
		if (term_2._vectorRepresentation.size() >_vectorRepresentation.size()) {
			return true;
		}

		for (long long int i = _vectorRepresentation.size() - 1; i >= 0; i--) {
			if (_vectorRepresentation[i] < term_2._vectorRepresentation[i]) {
				return true;
			}
			if (_vectorRepresentation[i] > term_2._vectorRepresentation[i]) {
				return false;
			}
		}
		return false;
	}


	friend void _plusVectors(BigInteger& term_1, const BigInteger& term_2) {
		int deposit = 0;
		size_t index = 0;
		for (; index < term_1._vectorRepresentation.size(); index++) {
			int digit_sum = term_1._vectorRepresentation[index] + deposit;
			if (index < term_2._vectorRepresentation.size()) {
				digit_sum += term_2._vectorRepresentation[index];
			}
			deposit = digit_sum / 10;
			term_1._vectorRepresentation[index] = (digit_sum % 10);
		}

		for (; index < term_2._vectorRepresentation.size(); index++) {
			int digit_sum = term_2._vectorRepresentation[index] + deposit;
			deposit = digit_sum / 10;
			term_1._vectorRepresentation.push_back(digit_sum % 10);
		}

		if (deposit == 1) {
			term_1._vectorRepresentation.push_back(deposit);
		}
	}



	friend void _minusVectors(BigInteger& term_1, const BigInteger& term_2) {
		int debt = 0;
		long long int begin_zero_prefix = 0;
		size_t index = 0;
		for (; index < term_2._vectorRepresentation.size(); index++) {
			int current_difference = term_1._vectorRepresentation[index] - term_2._vectorRepresentation[index] - debt;
			if (current_difference < 0) {
				current_difference += 10;
				debt = 1;
			}
			else {
				debt = 0;
			}
			if (current_difference != 0) {
				begin_zero_prefix = index + 1;
			}
			term_1._vectorRepresentation[index] = current_difference;
		}

		for (; index < term_1._vectorRepresentation.size(); index++) {
			int current_difference = term_1._vectorRepresentation[index] - debt;
			if (current_difference < 0) {
				current_difference += 10;
				debt = 1;
			}
			else {
				debt = 0;
			}
			if (current_difference != 0) {
				begin_zero_prefix = index + 1;
			}
			term_1._vectorRepresentation[index] = current_difference;
		}

		if (begin_zero_prefix == 0) {
			term_1._sign = 0;
			begin_zero_prefix = 1;
		}
		term_1._vectorRepresentation.resize(begin_zero_prefix);
	}

	bool _isZero() const {
		return (_vectorRepresentation.size() == 1 && _vectorRepresentation[0] == 0);
	}


	friend BigInteger _multiplyDigit(const BigInteger& term_1, int digit, long long int zeros) {
		int deposit = 0;
		BigInteger result;
		if (digit == 0) {
			return result;
		}
		result._vectorRepresentation = {};
		for (size_t i = 0; i < term_1._vectorRepresentation.size(); i++) {
			int digitMultiply = term_1._vectorRepresentation[i] * digit + deposit;
			deposit = digitMultiply / 10;
			digitMultiply = digitMultiply % 10;
			result._vectorRepresentation.push_back(digitMultiply);
		}
		if (deposit > 0) {
			result._vectorRepresentation.push_back(deposit);
		}
		vector<int> zerosVector(zeros);
		result._vectorRepresentation.insert(
			result._vectorRepresentation.begin(),
			zerosVector.begin(),
			zerosVector.end()
		);
		return result;
	}
};


BigInteger::BigInteger() {}
BigInteger::~BigInteger() {}
	
BigInteger::BigInteger(const int value): _sign(value < 0), _vectorRepresentation() {
	int standartInt = value;
	if (standartInt < 0) {
		standartInt = -standartInt;
	}
	if (standartInt == 0) {
		_vectorRepresentation = { 0 };
	}
	while (standartInt > 0) {
		_vectorRepresentation.push_back(standartInt % 10);
		standartInt = standartInt / 10;
	}
}

BigInteger::BigInteger(const string& value) {
	_sign = 0;
	_vectorRepresentation.resize(value.length());
	if (value[0] == '-') {
		_sign = 1;
		_vectorRepresentation.pop_back();
	}
	long long int begin_zero_prefix = 1;
	for (size_t i = 0; i <value.length() - _sign; i++) {
		_vectorRepresentation[i] = stoi(
			value.substr(value.length() - i - 1, 1)
		);
		if (_vectorRepresentation[i] != 0) {
			begin_zero_prefix = i + 1;
		}
	}
	_vectorRepresentation.resize(begin_zero_prefix);
	if (_isZero()) {
		_sign = 0;
	}
}

string BigInteger::toString() const {
	string stringRepresentation;
	if (_sign == 1) {
		stringRepresentation = "-";
	}
	for (long long int i = _vectorRepresentation.size() - 1; i >= 0; i--) {
		stringRepresentation += to_string(_vectorRepresentation[i]);
	}
	return stringRepresentation;
}


bool BigInteger::operator>=(const BigInteger& other) const {
	return *this > other || *this == other;
}


bool BigInteger::operator<=(const BigInteger& other) const {
	return *this < other || *this == other;
}


bool BigInteger::operator>(const BigInteger& other) const {
	if (*this == other) {
		return false;
	}
	if (_sign == 0 && other._sign == 1) {
		return true;
	}
	if (_sign == 1 && other._sign == 0) {
		return false;
	}
	if (_sign == 1 && other._sign == 1) {
		return _absLessThan(other);
	}
	return !_absLessThan(other);
}


bool BigInteger::operator<(const BigInteger& other) const {
	if (*this == other) {
		return false;
	}
	if (_sign == 1 && other._sign == 0) {
		return true;
	}
	if (_sign == 0 && other._sign == 1) {
		return false;
	}
	if (_sign == 1 && other._sign == 1) {
		return !_absLessThan(other);
	}
	return _absLessThan(other);
}


bool BigInteger::operator==(const BigInteger& other) const {
	if (_sign != other._sign) {
		return false;
	}
	if (_vectorRepresentation.size() != other._vectorRepresentation.size()) {
		return false;
	}
	for (size_t i = 0; i < other._vectorRepresentation.size(); i++) {
		if (_vectorRepresentation[i] != other._vectorRepresentation[i]) {
			return false;
		}
	}
	return true;
}


bool BigInteger::operator!=(const BigInteger& other) const {
	return !(*this == other);
}


BigInteger operator-(const BigInteger& term_1, const BigInteger& term_2) {
	BigInteger result;
	if (term_1._sign == term_2._sign) {
		if (term_1._absLessThan(term_2)) {
			result = term_2;
			_minusVectors(result, term_1);
			if (!result._isZero()) {
				result._sign = 1 - result._sign;
			}
			return result;
		}
		result = term_1;
		_minusVectors(result, term_2);
		return result;
	}
	result = term_1;
	_plusVectors(result, term_2);
	return result;
}


BigInteger operator+(const BigInteger& term_1, const BigInteger& term_2) {
	BigInteger result;
	if (term_1._sign == term_2._sign) {
		result = term_1;
		_plusVectors(result, term_2);
		return result;
	}
	if (term_1._absLessThan(term_2)) {
		result = term_2;
		_minusVectors(result, term_1);
		return result;
	}
	result = term_1;
	_minusVectors(result, term_2);
	return result;
}

BigInteger operator/(const BigInteger& term_1, const BigInteger& term_2) {
	return _division(term_1, term_2).first;
}


BigInteger operator%(const BigInteger& term_1, const BigInteger& term_2) {
	BigInteger result = _division(term_1, term_2).second;
	result._sign = term_1._sign;
	return result;
}


BigInteger operator*(const BigInteger& term_1, const BigInteger& term_2)  {
	BigInteger result;
	if (term_1._isZero() || term_2._isZero()) {
		return result;
	}
	for (size_t i = 0; i < term_2._vectorRepresentation.size(); i++) {
		result = result + _multiplyDigit(term_1, term_2._vectorRepresentation[i], i);
	}
	result._sign = (term_1._sign != term_2._sign);
	return result;
}

BigInteger& BigInteger::operator++(){
	BigInteger temporary;
	temporary._vectorRepresentation = { 1 };
	if (_sign == 0) {
		_plusVectors(*this, temporary);
	}
	else {
		_minusVectors(*this, temporary);
	}
	return *this;
}

BigInteger& BigInteger::operator--() {
	BigInteger temporary;
	temporary._vectorRepresentation = { 1 };
	if (_isZero()) {
		_sign = 1;
		_vectorRepresentation = { 1 };
		return *this;
	}
	if (_sign == 0) {
		_minusVectors(*this, temporary);
	}
	else {
		_plusVectors(*this, temporary);
	}
	return *this;
}

BigInteger BigInteger::operator++(int) {
	++*this;
	return *this;
}

BigInteger BigInteger::operator--(int) {
	--*this;
	return *this;
}


BigInteger BigInteger::operator-() const {
	BigInteger result = *this;
	if (!result._isZero()) {
		result._sign = 1 - result._sign;
	}
	return result;
}


BigInteger& BigInteger::operator+=(const BigInteger& term) {
	*this = *this + term;
	return *this;
}


BigInteger& BigInteger::operator-=(const BigInteger& term) {
	*this = *this - term;
	return *this;
}


BigInteger& BigInteger::operator*=(const BigInteger& term) {
	*this = *this * term;
	return *this;
}


BigInteger& BigInteger::operator/=(const BigInteger& term) {
	*this = *this / term;
	return *this;
}

BigInteger& BigInteger::operator%=(const BigInteger& term) {
	*this = *this % term;
	return *this;
}


BigInteger::operator bool() const {
	return !_isZero();
}


istream& operator>>(istream& input, BigInteger& value) {
	string stringRepresentation;
	input >> stringRepresentation;
	value = BigInteger(stringRepresentation);
	return input;
}

ostream& operator<<(ostream& stream, const BigInteger& value) {
	stream << value.toString();
	return stream;
}